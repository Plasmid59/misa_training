const axios = require('axios');

const axiosInstance = axios.create({
  baseURL: 'http://localhost:8081/fakeApi',
  validateStatus: () => {
    return true;
  }
});

module.exports = { axiosInstance };
