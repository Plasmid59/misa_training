const Router = require('express-promise-router');
const backend = require('./backend');

const router = new Router();

router.use('/example', backend);

module.exports = router;
