const Router = require('express-promise-router');
const { axiosInstance: axios } = require('../service');

const router = new Router();

router.get('/button1', async (req, res) => {
  const { data } = await axios.get('/exampleRoute/route1');

  res.send(data);
});

router.get('/button2/:greeting', async (req, res) => {
  const { greeting } = req.params;

  const { data } = await axios.get(`/exampleRoute/route2/${greeting}`);

  res.send(data);
});

router.get('/button3/:greeting', async (req, res) => {
  const { greeting } = req.params;

  const { data } = await axios.get(`/exampleRoute/route3/${greeting}`);

  res.send(data);
});

module.exports = router;
