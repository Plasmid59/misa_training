const Router = require('express-promise-router');

const router = new Router();

router.get('/route1', async (req, res) => {
  const response = '\'Ello Govna!';

  res.send(response);
});

router.get('/route2/:hello', async (req, res) => {
  const { hello } = req.params;
  const response = `You say goodbye, and I say ${hello}`;

  res.send(response);
});

router.get('/route3/:hello', async (req, res) => {
  const { hello } = req.params;
  const response = `Hello ${hello}`;

  res.send(response);
});

module.exports = router;
