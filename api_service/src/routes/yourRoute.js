const Router = require('express-promise-router');

const router = new Router();

// Challenge: Create a new route that can be called by the backend
// You can choose your own path for the route
// Use the exampleRoute.js file as an example

module.exports = router;
