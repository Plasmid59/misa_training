const Router = require('express-promise-router');
const exampleRoute = require('./exampleRoute');
const yourRoute = require('./yourRoute');

const router = new Router();

router.use('/exampleRoute', exampleRoute);
router.use('/yourRoute', yourRoute);

module.exports = router;
