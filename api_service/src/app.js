const bodyparser = require('body-parser');
const express = require('express');
const log4js = require('log4js');
const router = require('./routes/routes');
const cors = require('cors');

const app = express();
const logger = log4js.getLogger();
logger.level = 'debug';

app.use(cors());
app.use(bodyparser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyparser.json({ limit: '50mb', extended: true }));
app.use('/fakeApi', router);

app.listen(8081, () => logger.info('Server started on 8081'));
