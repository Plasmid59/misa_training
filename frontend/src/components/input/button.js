import React from 'react';
import axios from 'axios';
import endpoints from '../../enums/endpoints';

const buttonStyle = {
  width: '10%',
  height: '30pt',
  backgroundColor: 'blue',
  margin: '10pt',
  color: 'white',
  fontSize: '20pt',
  fontWeight: 'bold',
  borderRadius: '10pt',
  boxShadow: '20px 10px' 
}

const Button = ({ setResult, buttonValue, path, result }) => {
  const handleOnClick = async () => {
    if (result === ' ') {
      const { data } = await axios.get(`${endpoints.BACKEND_ENDPOINT}${path}`);
      setResult(data);
    } else {
      setResult(' ');
    }
  };
  
  return (
    <div>
      <input style={buttonStyle} type='button' onClick={handleOnClick} value={buttonValue} />
    </div>
  )
};

export default Button;
