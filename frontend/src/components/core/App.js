import React, { useState } from 'react';
import Button from '../input/button';
import Result from '../output/result';

const appStyle = {
  paddingTop: '10%',
  width: '100%',
  textAlign: 'center'
}

const App = () => {
  const [result1, setResult1] = useState(' ');
  const [result2, setResult2] = useState(' ');
  const [result3, setResult3] = useState(' ');
  const [result4, setResult4] = useState(' ');
  
  // TODO: Add in the path for 'Button 4' that points to YOUR backend route
  return (
    <div style={appStyle}>
      <Button setResult={setResult1} result={result1} buttonValue='Button 1' path='/button1' />
      <Result result={result1} />
      <Button setResult={setResult2} result={result2} buttonValue='Button 2' path='/button2/hello' />
      <Result result={result2} />
      <Button setResult={setResult3} result={result3} buttonValue='Button 3' path='/button3/there' />
      <Result result={result3} />
      <Button setResult={setResult4} result={result4} buttonValue='Button 4' />
      <Result result={result4} />
    </div>
  );
}

export default App;
